BINARY_NAME=main.out

.DEFAULT_GOAL := build

build:
	go build -ldflags="-s -w" -o ${BINARY_NAME} main.go

run:
	go run main.go
